package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class MainPage {
    private WebDriver driver;
    private WebDriverWait wait;
    private By codeTextAreaLocator = By.id("postform-text");
    private By titleFieldLocator = By.id("postform-name");
    private By expirationArrowLocator = By.xpath("//select[@id='postform-expiration']" +
            "/ancestor::div[1]//span[contains(@class,'arrow')]");
    private By expirationOptionsLocator = By.xpath("//ul[contains(@class,'results__options')]");
    private By submitButtonLocator = By.xpath("//button[@type='submit' and contains(@class, 'btn')" +
            " and contains(@class, '-big')]");

    public MainPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(3));
    }

    public MainPage openPastebinPage(String url) {
        driver.get(url);

        wait.until(ExpectedConditions.presenceOfElementLocated(codeTextAreaLocator));
        return this;
    }

    public MainPage fillTextArea(String code) {

        WebElement codeTextArea = driver.findElement(codeTextAreaLocator);
        codeTextArea.sendKeys(code);

        return this;
    }

    public MainPage fillTitle(String title) {
        WebElement titleField = driver.findElement(titleFieldLocator);
        titleField.sendKeys(title);

        return this;
    }

    public MainPage clickExpirationArrow() {
        WebElement pasteExpirationArrow = driver.findElement(expirationArrowLocator);
        pasteExpirationArrow.click();

        return this;
    }

    public MainPage selectExpirationPeriod(String period) {
        WebElement expirationList = driver.findElement(expirationOptionsLocator);
        List<WebElement> liElements = expirationList.findElements(By.tagName("li"));

        liElements.stream()
                .filter(e -> e.getText().contains(period))
                .findFirst()
                .get()
                .click();

        return this;
    }

    public ResultPage clickSubmitButton() {
        WebElement submitButton = driver.findElement(submitButtonLocator);
        submitButton.click();

        return new ResultPage(driver);
    }
}
