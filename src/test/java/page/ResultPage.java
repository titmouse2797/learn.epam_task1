package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ResultPage {
    private WebDriver driver;
    private WebDriverWait wait;

    private By successMessageLocatorText = By.xpath("//div[contains(@class, 'success')]");

    public ResultPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(3));
    }

    public boolean isPasteCreatedSuccessfully() {
        WebElement successMessageText = wait.until(ExpectedConditions.presenceOfElementLocated(successMessageLocatorText));
        return successMessageText.isDisplayed();
    }
}
