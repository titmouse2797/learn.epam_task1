package test;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import page.MainPage;
import page.ResultPage;

public class CreateNewPasteTest {
    private WebDriver driver;
    private MainPage mainPage;

    // test data that could in future placed in data providers
    private String code = "Hello from WebDriver";
    private String expirationPeriod = "10 Minutes";
    private String title = "helloweb";

    // main page url that could in future placed in properties file
    private String mainPageUrl = "https://pastebin.com/";

    @Before
    public void setUp() {
        driver = new ChromeDriver();
        mainPage = new MainPage(driver);
    }

    @Test
    public void testCreateNewPaste() {
        ResultPage resultPage = mainPage
                .openPastebinPage(mainPageUrl)
                .fillTextArea(code)
                .clickExpirationArrow()
                .selectExpirationPeriod(expirationPeriod)
                .fillTitle(title)
                .clickSubmitButton();

        Assert.assertTrue("Failed to create a new paste.", resultPage.isPasteCreatedSuccessfully());
    }

    @After
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
